using System;
using System.Threading;
using System.Runtime.CompilerServices;

// https://www.google.com/search?q=C%23+like+java+thread+synchronized+&client=firefox-b-d&sxsrf=ALeKk0360gJT3DR6LQ1ZEQ1RKZcQl6_7mw%3A1618797883756&ei=O-V8YLDfLdHB5OUPru-4qA0&oq=C%23+like+java+thread+synchronized+&gs_lcp=Cgdnd3Mtd2l6EAM6BwgjELADECc6BwgAEEcQsAM6CAgAEAcQHhATOgIIADoGCAAQBxAeOgUIABDLAToGCAAQFhAeOgQIABATOgoIABAIEAcQHhATOggIABANEB4QE1DMnQlYm-AJYJPhCWgCcAF4AIABrgGIAdMQkgEEMC4xN5gBAKABAaoBB2d3cy13aXrIAQnAAQE&sclient=gws-wiz&ved=0ahUKEwjw6MuGnInwAhXRILkGHa43DtUQ4dUDCA0&uact=5
// https://www.knowledgehut.com/tutorials/csharp/csharp-synchronization
// https://www.javatpoint.com/c-sharp-thread-synchronization
// https://www.geeksforgeeks.org/how-to-create-threads-in-c-sharp/

namespace Sistemas_Distribuidos
{
    class ContaBancaria
    {
        static void Main(string[] args)
        {
            Account account = new Account();
            Withdraw withdraw = new Withdraw(account);
            Deposit deposit = new Deposit(account); 

            Thread withDrawThread = new Thread(new ThreadStart(withdraw.Recal));
            Thread depositThered = new Thread(new ThreadStart(deposit.Depositi));

            withDrawThread.Name = "Withdraw";
            depositThered.Name = "\t Deposit";

            // Exemplo de priority 
            //thread1.Priority = (ThreadPriority.Highest);
            //thread2.Priority = (ThreadPriority.Lowest);

            withDrawThread.Start();
            depositThered.Start();

            withDrawThread.Join();
            depositThered.Join();


            Console.WriteLine("Saldo " + account.balance);

        }
    }

    public class Account
    {
        public int balance = 0;
    }

    public class Withdraw
    {
        private Account conta;

        public Withdraw(Account conta)
        {
            this.conta = conta; 
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void Recal()
        {
            int[] values = { 10, 20, 30, 40, 50, 60 };

            for (int i = 0; i < values.Length; i++)
            {
                conta.balance -= values[i];
            }
        }
    }

    public class Deposit
    {
        private Account conta;

        public Deposit(Account conta)
        {
            this.conta = conta;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void Depositi()
        {
            int[] values = { 40, 50, 60, 10, 20, 30 };

            for (int i = 0; i < values.Length; i++)
            {
                conta.balance += values[i];
            }
        }
    }


}
