﻿// using System;
// using System.Threading;

// namespace Sistemas_Distribuidos
// {
//     class Program
//     {
//         static void Main(string[] args)
//         {
//             Console.WriteLine("Inicio da thread main");

//             Thread thread1 = new Thread(new ThreadStart(ThreadProc));
//             Thread thread2 = new Thread(new ThreadStart(ThreadProc));

//             thread1.Name = "thread1";
//             thread2.Name = "\t thread2";

//             // Exemplo de priority 
//             //thread1.Priority = (ThreadPriority.Highest);
//             //thread2.Priority = (ThreadPriority.Lowest);

//             thread1.Start();
//             thread2.Start();


//             Console.WriteLine("Término da thread main");

//         }

//         public static void ThreadProc()
//         {            
//             Console.WriteLine(Thread.CurrentThread.Name + " Rodando");

//             for (int i = 0; i < 100; i++)
//             {
//                 Console.WriteLine(Thread.CurrentThread.Name + ": " + i);
//                 // Exemplo de yield
//                 //Thread.Yield();
                
//             }
//             Console.WriteLine(Thread.CurrentThread.Name + " End ****");
//         }
//     }
// }
